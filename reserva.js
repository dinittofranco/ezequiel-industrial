let valorReserva = 0;
let precio = 0;
let valorFinal = 0;

$(function() {
    $("#mySelect").change(function() {
        let region = $("#mySelect").val();
        $("#myInput").val(region);
        RenderInputs(region);
    });
});

function RenderInputs(region) {

    $("#content").html("");
    switch (region) {
        case "Buenos Aires":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="Palermo">Palermo</option>' +
                '<option value="Puerto Madero">Puerto Madero</option>' +
                '<option value="San Telmo">San Telmo</option> </select>'
            );
            $("#content").append("</div>");
            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val()
                valueCiudad(ciudad);
            });
            break;

        case "Litoral":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="Misiones">Misiones</option>' +
                '<option value="Entre Rios">Entre Ríos</option> </select>'
            );

            $("#content").append("</div>");
            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val();
                valueCiudad(ciudad);
            });
            break;

        case "Centro":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="Capilla del Monte">Capilla del Monte</option>' +
                '<option value="Cordoba">Cordoba</option>'
            );
            $("#content").append("</div>");

            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val();
                valueCiudad(ciudad);
            });
            break;

        case "Cuyo":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="San Luis">San Luis</option>' +
                '<option value="San Juan">San Juan</option>' +
                '<option value="La Rioja">La Rioja</option>'
            );
            $("#content").append("</div>");

            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val();
                valueCiudad(ciudad);
            });
            break;

        case "Patagonia":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="Rio Negro">Río Negro</option>' +
                '<option value="La Pampa">La Pampa</option>' +
                '<option value="Neuquen">Neuquén</option>'
            );
            $("#content").append("</div>");

            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val();
                valueCiudad(ciudad);
            });
            break;

        case "Noroeste":
            $("#content").append('<div class="col-6">');
            $("#content").append(
                "<label>Provincia / Ciudad</label>" +
                '<select id="ciudad" class="form-control">' +
                "<option disabled selected>Seleccione un destino</option>" +
                '<option value="Santiago del Estero">Santiago del Estero</option>' +
                '<option value="Tucuman">Tucuman</option>' +
                '<option value="Jujuy">Jujuy</option>'
            );
            $("#content").append("</div>");

            $("#ciudad").change(function() {
                let ciudad = $("#ciudad").val();
                valueCiudad(ciudad);
            });
            break;
    }

    function valueCiudad(ciudad) {
    
        switch (region) {
            case 'Litoral':
                precio = 15000
                break;
            case 'Buenos Aires':
                precio = 3000
                break;
            case 'Centro':
                precio = 7300
                break;
            case 'Cuyo':
                precio = 12000
                break;
            case 'Patagonia':
                precio = 33000
                break;
            case 'Noroeste':
                precio = 35000
                break;

        }

        $("#reserva").change(function() {
            let valor = $("#reserva").val();
            $("#reserva").val(valor);
            valorReserva = Number(valor);

        });

        // Grab the template script
        let theTemplateScript = $("#address-template").html();
        // Compile the template
        let theTemplate = Handlebars.compile(theTemplateScript);

        // Define our data object
        let context = {
            "ciudad": ciudad,
            "region": region,
            "precio": precio,
        };

        // Pass our data to the template
        let theCompiledHtml = theTemplate(context);

        // Add the compiled html to the page
        $('.content-placeholder').html(theCompiledHtml);

    }

    $("#pagar").click(function() {

        (valorReserva > precio) ? alert('La reserva supera el precio del viaje') : calculo();

    });

    const calculo = () => {
        valorFinal = Number(precio - valorReserva);
        precio = valorFinal
        
        $("#precioVuelo").html('<p class="note note-success">' + '<strong>'+'Precio: $' + precio + '</strong>'+ '</p>');
        
        if (valorFinal === 0) {
            alert('Viaje totalmente pago')
            valorFinal = 0;
            location.reload()
            return

        }

    }

    

}

